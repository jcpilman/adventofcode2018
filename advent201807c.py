# happy advent
# 7
# part 2
a = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""

class Step:
    def __init__(self, name, req):
        self.name = name
        # list of prerequisites
        self.req = req
        # time per step
        self.tps = 0

# read list of prerequisites and 
# build dict

# logic = dict{index = ['A'-'Z']: value = list[prerequisites = ['A'-'Z']]}
logic = {}
# completed = list['A'-'Z']
completed = []
# inprogress = dict{index = ['A'-'Z']: value = [True|False]}
inprogress = []
# sequence = string 'A- last letter'
sequence = ''
# c = list[ input lines]
c = a.split('\n')
# d = one line of input
for d in c:
    # b = list of words from input line
    b = d.split()
    step_name = b[7]
    step_req = b[1]
    if step_name in logic:
        logic[step_name].append(step_req)
    else:
        logic[step_name] = [step_req]
    if step_name not in sequence:
        sequence += step_name
    if step_req not in sequence:
        sequence += step_req

sequence = sorted(sequence)

def letter_time(a):
    # return index of letter as additional time in seconds
    return ord(a) - 64

# loop through the sorted sequence until every logic is solved
# a logic depends on the prerequitites all being true

total_time = 0
final_seq = ''
final_test = False
while not final_test:
    for item in sequence:
        if item not in completed:
            if item not in logic:
                # assign it to a logic solver
                print('no conditions for', item)
                completed.append(item)
                break
            else:
                test = True
                for cond in logic[item]:
                    if cond not in completed:
                        test = False
                        # maybe put break here?
                if test:
                    completed.append(item)
                    break
    if item in completed:
        final_seq += item
        if len(final_seq) == len(sequence):
            final_test = True
print(final_seq)
for item in final_seq:
    print(letter_time(item))
