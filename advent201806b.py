# happy advent
# part 2
a = """84, 212
168, 116
195, 339
110, 86
303, 244
228, 338
151, 295
115, 49
161, 98
60, 197
40, 55
55, 322
148, 82
86, 349
145, 295
243, 281
91, 343
280, 50
149, 129
174, 119
170, 44
296, 148
152, 160
115, 251
266, 281
269, 285
109, 242
136, 241
236, 249
338, 245
71, 101
254, 327
208, 231
289, 184
282, 158
352, 51
326, 230
88, 240
292, 342
352, 189
231, 141
280, 350
296, 185
226, 252
172, 235
137, 161
207, 90
101, 133
156, 234
241, 185"""

# make a list of spot coordinates
b = a.split('\n')
cs = []
minx = miny = 1000
maxx = maxy = 0
for d in b:
    c = [int(d.partition(', ')[0]), int(d.partition(', ')[2])]
    cs.append(c)
    x = int(d.partition(', ')[0])
    y = int(d.partition(', ')[2])
    minx = min(minx, x)
    miny = min(miny, y)
    maxx = max(maxx, x)
    maxy = max(maxy, y)


print(minx, miny, maxx, maxy, len(cs))

# max area to consider (40, 44) x (352 - 40, 350 - 44)
# make a dict of closest points

x1 = -200
x2 = 600

close_points = 0
for x in range(x1, x2):
    for y in range(x1, x2):
        cp = -1
        distsum = 0
        for i, c in enumerate(cs):
            # find point closest to x, y
            dist = abs(x - c[0]) + abs(y - c[1])
            distsum += dist

        if distsum < 10000:
            close_points += 1
print(close_points)
# second pass
x1 = -300
x2 = 700

close_points2 = 0
for x in range(x1, x2):
    for y in range(x1, x2):
        cp = -1
        distsum = 0
        for i, c in enumerate(cs):
            # find point closest to x, y
            dist = abs(x - c[0]) + abs(y - c[1])
            distsum += dist

        if distsum < 10000:
            close_points2 += 1
print(close_points2)
