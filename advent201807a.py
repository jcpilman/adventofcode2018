# happy advent
# 7
# part 1
a = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""

logic = {}
completed = {}
sequence = ''
c = a.split('\n')
for d in c:
    b = d.split()
    if b[7] in logic:
        logic[b[7]].append(b[1])
    else:
        logic[b[7]] = [b[1]]
    completed[b[1]] = False
    completed[b[7]] = False
    if b[7] not in sequence:
        sequence += b[7]
    if b[1] not in sequence:
        sequence += b[1]

sequence = sorted(sequence)

# print('sequence', sequence)
# print('logic', logic)
# print('completed', completed)
# print()

final_seq = ''
final_test = False
while not final_test:
    for item in sequence:
        if not completed[item]:
            if item not in logic:
                print('no conditions for', item)
                completed[item] = True
                break
            else:
                print(completed)
                test = True
                for cond in logic[item]:
                    # print(cond, logic[cond])
                    if not completed[cond]:
                        test = False
                if test:
                    completed[item] = True
                    break
    if completed[item]:
        final_seq += item
        if len(final_seq) == len(sequence):
            final_test = True
print(final_seq)
