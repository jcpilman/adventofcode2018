# happy advent
# 7
# part 2
a = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""

a = """Step B must be finished before step X can begin.
Step V must be finished before step F can begin.
Step K must be finished before step C can begin.
Step S must be finished before step D can begin.
Step C must be finished before step A can begin.
Step H must be finished before step X can begin.
Step Q must be finished before step W can begin.
Step X must be finished before step F can begin.
Step J must be finished before step R can begin.
Step D must be finished before step O can begin.
Step F must be finished before step P can begin.
Step M must be finished before step Z can begin.
Step R must be finished before step I can begin.
Step Y must be finished before step O can begin.
Step G must be finished before step Z can begin.
Step Z must be finished before step P can begin.
Step O must be finished before step L can begin.
Step A must be finished before step P can begin.
Step U must be finished before step L can begin.
Step L must be finished before step W can begin.
Step P must be finished before step W can begin.
Step I must be finished before step W can begin.
Step E must be finished before step N can begin.
Step W must be finished before step N can begin.
Step T must be finished before step N can begin.
Step G must be finished before step E can begin.
Step K must be finished before step T can begin.
Step I must be finished before step T can begin.
Step V must be finished before step H can begin.
Step W must be finished before step T can begin.
Step M must be finished before step A can begin.
Step C must be finished before step W can begin.
Step B must be finished before step Y can begin.
Step Y must be finished before step N can begin.
Step L must be finished before step N can begin.
Step M must be finished before step R can begin.
Step L must be finished before step I can begin.
Step J must be finished before step N can begin.
Step K must be finished before step M can begin.
Step O must be finished before step U can begin.
Step P must be finished before step N can begin.
Step Y must be finished before step I can begin.
Step V must be finished before step Q can begin.
Step H must be finished before step R can begin.
Step M must be finished before step P can begin.
Step K must be finished before step L can begin.
Step J must be finished before step A can begin.
Step D must be finished before step F can begin.
Step Q must be finished before step P can begin.
Step C must be finished before step H can begin.
Step U must be finished before step I can begin.
Step A must be finished before step T can begin.
Step C must be finished before step P can begin.
Step U must be finished before step T can begin.
Step O must be finished before step T can begin.
Step O must be finished before step I can begin.
Step S must be finished before step I can begin.
Step Z must be finished before step E can begin.
Step Y must be finished before step T can begin.
Step K must be finished before step O can begin.
Step O must be finished before step A can begin.
Step Z must be finished before step T can begin.
Step Z must be finished before step U can begin.
Step U must be finished before step P can begin.
Step P must be finished before step I can begin.
Step S must be finished before step W can begin.
Step S must be finished before step P can begin.
Step S must be finished before step Q can begin.
Step C must be finished before step E can begin.
Step G must be finished before step U can begin.
Step D must be finished before step L can begin.
Step K must be finished before step S can begin.
Step R must be finished before step O can begin.
Step C must be finished before step G can begin.
Step V must be finished before step G can begin.
Step A must be finished before step W can begin.
Step Z must be finished before step O can begin.
Step J must be finished before step O can begin.
Step F must be finished before step E can begin.
Step U must be finished before step E can begin.
Step E must be finished before step W can begin.
Step M must be finished before step O can begin.
Step C must be finished before step U can begin.
Step G must be finished before step P can begin.
Step C must be finished before step I can begin.
Step Z must be finished before step A can begin.
Step C must be finished before step J can begin.
Step Q must be finished before step R can begin.
Step E must be finished before step T can begin.
Step F must be finished before step Y can begin.
Step Z must be finished before step N can begin.
Step I must be finished before step N can begin.
Step X must be finished before step E can begin.
Step I must be finished before step E can begin.
Step Q must be finished before step O can begin.
Step R must be finished before step L can begin.
Step K must be finished before step W can begin.
Step Y must be finished before step L can begin.
Step M must be finished before step I can begin.
Step F must be finished before step O can begin.
Step A must be finished before step E can begin."""

class Step:
    def __init__(self, name, req):
        self.name = name
        # list of prerequisites
        self.req = req
        # time per step
        self.tps = 0

# read list of prerequisites and 
# build dict
# number of logic solvers
solver_max = 5
# logic = dict{index = ['A'-'Z']: value = list[prerequisites = ['A'-'Z']]}
logic = {}
# individual time keeper for each step
my_time = {}
# completed = list['A'-'Z']
completed = []
# inprogress = dict{index = ['A'-'Z']: value = [True|False]}
inprogress = set()
# sequence = string 'A- last letter'
sequence = ''
# c = list[ input lines]
c = a.split('\n')
# d = one line of input
for d in c:
    # b = list of words from input line
    b = d.split()
    step_name = b[7]
    step_req = b[1]
    if step_name in logic:
        logic[step_name].append(step_req)
    else:
        logic[step_name] = [step_req]
    if step_name not in sequence:
        sequence += step_name
    if step_req not in sequence:
        sequence += step_req

sequence = sorted(sequence)

def letter_time(a):
    # return index of letter as additional time in seconds
    return ord(a) - 64

def check_conditions(item, reqs, all_done):
    # logic[item] is a list of condition states
    print('checking conditions for', item, '=', reqs)
    test = True
    for item in reqs:
        if item not in all_done:
            test =  False
            break
    return test

# loop through the sorted sequence until every logic is solved
# a logic depends on the prerequitites all being true
min_time = 60
total_time = 0
final_seq = ''
final_test = False
while not final_test:
    # see what is ready for a logic solver
    for item in sequence:
        if item not in inprogress and item not in completed and len(inprogress) < solver_max:
            # len(inprogress) is the number of active logic solvers
            if item not in logic:
                # assign it to a logic solver now because there are no conditions
                inprogress.add(item)
                my_time[item] = total_time + letter_time(item) + min_time
                print('no conditions for', item, my_time[item])
                # break
            else:
                # if conditions are right assign it to a logic solver
                if check_conditions(item, logic[item], completed):
                    inprogress.add(item)
                    my_time[item] = total_time + letter_time(item) + min_time
                    print('logic[' + item + '] =', logic[item], my_time[item]) 
                    print('completed =', completed)       

    # count time for each item in logic solvers
    temp_inprogress = sorted(inprogress)
    print('temp_inprogress ', temp_inprogress)
    for item in temp_inprogress:
        # see if time is up
        if total_time >= my_time[item] - 1:
            completed.append(item)
            inprogress.remove(item)
            final_seq += item
    if len(final_seq) == len(sequence):
        final_test = True
    # print status line
    stl = str(total_time)
    stl += '\t'.join(inprogress)
    print('stl =', stl)

    total_time += 1


print(final_seq, total_time)
