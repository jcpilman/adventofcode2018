# happy advent
import math

a = """84, 212
168, 116
195, 339
110, 86
303, 244
228, 338
151, 295
115, 49
161, 98
60, 197
40, 55
55, 322
148, 82
86, 349
145, 295
243, 281
91, 343
280, 50
149, 129
174, 119
170, 44
296, 148
152, 160
115, 251
266, 281
269, 285
109, 242
136, 241
236, 249
338, 245
71, 101
254, 327
208, 231
289, 184
282, 158
352, 51
326, 230
88, 240
292, 342
352, 189
231, 141
280, 350
296, 185
226, 252
172, 235
137, 161
207, 90
101, 133
156, 234
241, 185"""

# make a list of coordinates
b = a.split('\n')
cs = []
minx = miny = 1000
maxx = maxy = 0
for d in b:
    c = [int(d.partition(', ')[0]), int(d.partition(', ')[2])]
    cs.append(c)
    x = int(d.partition(', ')[0])
    y = int(d.partition(', ')[2])
    minx = min(minx, x)
    miny = min(miny, y)
    maxx = max(maxx, x)
    maxy = max(maxy, y)


print(minx, miny, maxx, maxy, len(cs))

# max area to consider (40, 44) x (352 - 40, 350 - 44)
# make a dict of closest points

# try twice and compare answers
# eliminate any that change
x1 = -200
x2 = 600

cps = {}
for x in range(x1, x2):
    for y in range(x1, x2):
        cp = -1
        test_dist = 1000000
        disq = 1000000
        for i, c in enumerate(cs):
            # find point closest to x, y
            # dist = math.sqrt(((x - c[0]) * (x - c[0])) + ((y - c[1]) * (y - c[1])))
            dist = abs(x - c[0]) + abs(y - c[1])
            # check equidistants
            if dist == test_dist:
                # flag for disqualifications
                disq = dist
            if dist < test_dist:
                # closer point
                cp = i
                test_dist = dist
        if test_dist < disq:
            cps[(x, y)] = cp

# add up number of closest points for each item in list c
spot = {}
for x in range(x1, x2):
    for y in range(x1, x2):
        if (x, y) in cps:
            if cps[(x, y)] not in spot:
                spot[cps[(x, y)]] = 0
            spot[cps[(x, y)]] += 1

# find the greatest number of closest coordinates
# and make a sorted list of close numbers
maxcloses = 0
all_close = []
for i in range(50):
    if spot[i] > maxcloses:
        maxcloses = spot[i]
    all_close.append(spot[i])
print(maxcloses)
print(spot)
print(sorted(all_close))
print()
# second pass
x1 = -300
x2 = 700

cps = {}
for x in range(x1, x2):
    for y in range(x1, x2):
        cp = -1
        test_dist = 1000000
        disq = 1000000
        for i, c in enumerate(cs):
            # find point closest to x, y
            # dist = math.sqrt(((x - c[0]) * (x - c[0])) + ((y - c[1]) * (y - c[1])))
            dist = abs(x - c[0]) + abs(y - c[1])
            # check equidistants
            if dist == test_dist:
                # flag for disqualifications
                disq = dist
            if dist < test_dist:
                # closer point
                cp = i
                test_dist = dist
        if test_dist < disq:
            cps[(x, y)] = cp

# add up number of closest points for each item in list c
spot2 = {}
for x in range(x1, x2):
    for y in range(x1, x2):
        if (x, y) in cps:
            if cps[(x, y)] not in spot2:
                spot2[cps[(x, y)]] = 0
            spot2[cps[(x, y)]] += 1

# find the greatest number of closest coordinates
# and make a sorted list of close numbers
maxcloses = 0
all_close = []
for i in range(50):
    if spot[i] == spot2[i]:
        print(i, ':', spot[i], '=', spot2[i])
    else:
        print(i, ':', spot[i], '!=', spot2[i], '*')

