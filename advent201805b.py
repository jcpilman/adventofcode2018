a = """removing a 10640
removing b 10760
removing c 10686
removing d 10658
removing e 10666
removing f 10652
removing g 10624
removing h 10704
removing i 10662
removing j 10688
removing k 10708
removing l 10694
removing m 10728
removing n 10650
removing o 10686
removing p 10700
removing q 10690
removing r 6948
removing s 10626
removing t 10682
removing u 10660
removing v 10612
removing w 10696
removing x 10664
removing y 10714
removing z 10616"""

b = a.split('\n')
c = [d.split()[2] for d in b]

for e in sorted(c):
    print(e)
